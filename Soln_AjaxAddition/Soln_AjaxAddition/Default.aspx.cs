﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_AjaxAddition
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtnum1.Text!=null && txtnum2.Text!= null)
            {
                int num1 = Int32.Parse(txtnum1.Text);
                int num2 = Int32.Parse(txtnum2.Text);
                int result = num1 + num2;
                lblresult.Text = result.ToString();
            }
            else
            {
                lblresult.Text = "Enter numbers properly";
            }
        }
    }
}