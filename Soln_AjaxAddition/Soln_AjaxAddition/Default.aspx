﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_AjaxAddition.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Addition task</title>
    <link href="Css/css.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div >
    
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate >
                <table>
                    <tr>
                    <td> 
                <asp:TextBox ID="txtnum1" runat="server" CssClass="txt"  placeholder="number1"  /><br />
                    </td>
                       </tr>

                    <tr>
                    <td> 
                <asp:TextBox ID="txtnum2" runat="server" CssClass="txt"  placeholder="number2"  /><br />
                    </td>
                       </tr>

                    <tr>
                    <td> 
                <asp:Button ID="btnsubmit" runat="server" Text="Calculate" CssClass="btn btn2" OnClick="btnsubmit_Click" />
                    </td>
                       </tr>

                    <tr>
                        <td>
                            <asp:Label ID="lblresult" runat="server" />
                        </td>
                    </tr>

                </table>
                

            </ContentTemplate>


        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
